#include "racedashlog.h"
#include <stdlib.h>
#include <string.h>


#define LOG_HEADER_MAGIC     0x52444C42 // 'RLDB'
#define LOG_TIME_INDEX_MAGIC 0x4C475449 // 'LGTI'

#define LOG_VERSION      0x000002

//-----------------------------------------------
RacedashLog::RacedashLog(const char *fname) :
    Log()
{
    FILE *inf = fopen(fname, "rt");
    if (inf == NULL) return;

    // Check the file and make sure the magic is correct
    LogHeader header;
    fread(&header, sizeof(header), 1, inf);
    if (header.magic != LOG_HEADER_MAGIC)
    {
        printf("Log magic mismatch, read 0x%08X shd have been 0x%08X\n", header.magic, LOG_HEADER_MAGIC);
        return;
    }

    if (header.version != LOG_VERSION)
    {
        printf("Log version does not match should be %d, was %d\n", LOG_VERSION, header.version);
        return;
    }
    // Load the variable info
    unsigned a;
    for (a = 0; a < header.numVariables; a++)
    {
        LogHeaderVariable var;
        fread(&var, sizeof(var), 1, inf);
        LogItem item;
        item.name    = strdup(var.name);
        item.units   = strdup(var.units);
        item.scale   = 1.0 / var.scale;
        item.type    = var.type;
        item.id      = var.id;
        item.display = true;
        mLogItems.push_back(item);
    }

    // load the samples
    bool done = false;
    while (!feof(inf) && !done)
    {
        uint32_t magic;
        fread(&magic, sizeof(magic), 1, inf);
        fseek(inf, -sizeof(magic), SEEK_CUR);

        switch(magic)
        {
            case LOG_TIME_INDEX_MAGIC:
                done = !LoadLogTimeIndex(inf);
                break;

            default:
                printf("Got a bad Magic: 0x%08X\n", magic);
                done = true;
                break;
        }
    }

    fclose(inf);
}

//-----------------------------------------------
RacedashLog::~RacedashLog()
{
}

//-----------------------------------------------
bool RacedashLog::LoadLogTimeIndex(FILE *inf)
{
    ++mTotalTimeIndices;
    struct LogTimeIndex lti;
    fread(&lti, sizeof(lti), 1, inf);

    unsigned a;
    for (a = 0; a < lti.numVariables; a++)
    {
        DataLoggerID id;
        fread(&id, sizeof(id), 1, inf);

        int index = FindLogItemByID(id);
        if (index < 0) return false;

        LogItem &li = mLogItems[index];
        double out = 0;
        switch (li.type)
        {
            case Double:
            {
                double v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case Float:
            {
                float v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case U32:
            {
                uint32_t v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case S32:
            {
                int32_t v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case U16:
            {
                uint16_t v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case S16:
            {
                int16_t v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case U8:
            {
                uint8_t v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case S8:
            {
                int8_t v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
        }

        Sample s;
        s.time = lti.time * 0.00001;
        s.value = out * li.scale;
        li.samples.push_back(s);
        ++mTotalSamples;
    }
    return true;
}

