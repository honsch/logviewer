#ifndef LOGVIEWER_H
#define LOGVIEWER_H

#include <QMainWindow>
#include <QListWidgetItem>

#include "log.h"
#include "racedashlog.h"
#include "toothlogger.h"

namespace Ui {
class LogViewer;
}

class LogViewer : public QMainWindow
{
    Q_OBJECT

public:
    explicit LogViewer(QWidget *parent = 0);
    ~LogViewer();

private slots:
    void on_actionOpen_triggered();

    void on_itemList_itemChanged(QListWidgetItem *item);

private:
    Ui::LogViewer *ui;

    Log *mLog;

    void UpdateGraph(void);
};

#endif // LOGVIEWER_H
