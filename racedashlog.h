#ifndef RACEDASHLOG_H
#define RACEDASHLOG_H

#include "log.h"

//--------------------------------------------
class RacedashLog : public Log
{
  public:
    RacedashLog(const char *fname);
   ~RacedashLog();

  private:

    bool LoadLogTimeIndex(FILE *inf);

    //----------------------------------------------------
    struct LogHeaderVariable
    {
        char         name[64];
        char         units[64];
        double       scale;
        uint32_t     type;
        DataLoggerID id;
    };

    //----------------------------------------------------
    struct LogHeader
    {
        uint32_t magic;
        uint32_t version;
        uint32_t numVariables;
        uint32_t _pad;
        LogHeaderVariable _variables[0];
    };

    //----------------------------------------------------
    struct LogTimeIndex
    {
        LogTimeIndex(uint64_t t = 0) : time(t) {}
        uint32_t magic;
        uint32_t numVariables;
        uint64_t time;
        uint8_t  data[0];
    };
};

#endif // RACEDASHLOG_H
