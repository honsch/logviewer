#include "toothlogger.h"
#include <stdlib.h>
#include <string.h>

/*********
Loading a tooth log we only really care about the first five elements
PriLevel	SecLevel	Trigger	Sync	RefTime	MaxTime	ToothTime	Time
Flag	Flag	Flag	Flag	ms	ms	ms	ms
0.0,1.0,1.0,1.0,30693.984,30693.984,NaN,30693.984
1.0,1.0,0.0,1.0,30694.236,30694.236,0.252,30694.236
0.0,1.0,0.0,1.0,30694.717,30694.717,0.48,30694.717
.
.
.
MARK 000
0.0,1.0,0.0,1.0,33911.17,33911.17,3161.752,33911.17
1.0,1.0,0.0,1.0,33911.547,33911.547,0.376,33911.547
0.0,1.0,0.0,1.0,33912.074,33912.074,0.528,33912.074
.
.
.
EOF

***/
//-----------------------------------------------
ToothLog::ToothLog(const char *fname) :
    Log()
{
    FILE *inf = fopen(fname, "rt");
    if (inf == NULL) return;

    char line[256];
    unsigned lineCount = 0;
    while (!feof(inf))
    {
        ++lineCount;
        char *sret = fgets(line, 256, inf);
        if (sret == NULL) break;

        char *items[32];
        unsigned itemCount = ParseLine(line, items);
        if (lineCount < 3)
        {
            if (itemCount != 8)
            {
                printf("Line %d had %d items!\n", lineCount, itemCount);
                break;
            }

            if (lineCount == 1)
            {
                unsigned a;
                for (a = 0; a < 4; a++)
                {
                    LogItem item;
                    item.name    = strdup(items[a]);
                    item.units   = NULL;
                    item.scale   = 1.0;
                    item.type    = 0;
                    item.id      = a;
                    item.display = true;
                    mLogItems.push_back(item);
                }
                continue;
            }
            else if (lineCount == 2)
            {
                unsigned a;
                for (a = 0; a < 4; a++)
                {
                    mLogItems[a].units = strdup(items[a]);
                }
                continue;
            }
        }

        if (itemCount != 8)
        {
            printf("Line %d has the wrong number of items: %d, skipping.\n", lineCount, itemCount);
            continue;
        }

        Sample s;
        s.time = atof(items[4]) / 1000.0;// convert to seconds
        mTotalSamples += 4;
        mTotalTimeIndices += 1;

        unsigned a;
        for (a = 0; a < 4; a++)
        {
            s.value = atof(items[a]) + (5.0 - (a * 1.25));
            if (!mLogItems[a].samples.empty())
            {
                Sample prev = mLogItems[a].samples.back();
                if (s.value != prev.value)
                {
                    prev.time = s.time - 0.000001;
                    mLogItems[a].samples.push_back(prev);
                }
            }
            mLogItems[a].samples.push_back(s);
        }
    }
    fclose(inf);
}

//-----------------------------------------------
ToothLog::~ToothLog()
{

}

//-----------------------------------------------
bool IsSep(char c)
{
    if (c <= 32) return true;
    if (c == ',') return true;

    return false;
}

//-----------------------------------------------
unsigned ToothLog::ParseLine(char *line, char *items[])
{
    if (line == NULL) return 0;

    unsigned count = 0;
    unsigned len = strlen(line);

    unsigned pos;
    bool prevIsSep = true;
    for (pos = 0; pos < len; pos++)
    {
        bool isSep = IsSep(line[pos]);
        if (isSep)
        {
            line[pos] = 0;
        }
        else if (prevIsSep)
        {
            items[count] = &line[pos];
            ++count;
        }
        prevIsSep = isSep;
    }
    return count;
}
