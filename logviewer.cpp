#include "logviewer.h"
#include "ui_logviewer.h"

#include <QFileDialog>
#include <QString>
#define NUM_BRUSHES 14
QBrush sBrushes[NUM_BRUSHES] =
{
    QBrush(QColor(  0,   0,   0)),
    QBrush(QColor(255,   0,   0)),
    QBrush(QColor(0,   255,   0)),
    QBrush(QColor(0,     0, 255)),
    QBrush(QColor(255, 255,   0)),
    QBrush(QColor(  0, 255, 255)),
    QBrush(QColor(255,   0, 255)),
    QBrush(QColor(128, 128, 128)),
    QBrush(QColor(128,   0,   0)),
    QBrush(QColor(  0, 128,   0)),
    QBrush(QColor(  0,   0, 128)),
    QBrush(QColor(128, 128,   0)),
    QBrush(QColor(  0, 128, 128)),
    QBrush(QColor(128,   0, 128))
};

//------------------------------------------------------
LogViewer::LogViewer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LogViewer),
    mLog(NULL)
{
    ui->setupUi(this);

    // configure bottom axis to show date instead of number:
    QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);
    dateTicker->setDateTimeSpec(Qt::UTC);
    dateTicker->setDateTimeFormat("HH:mm:ss.zzzzz");
    ui->customPlot->xAxis->setTicker(dateTicker);

    ui->customPlot->yAxis->setNumberPrecision(12);
    // QDateTime

    ui->customPlot->setInteraction(QCP::iRangeDrag, true);
    ui->customPlot->setInteraction(QCP::iRangeZoom, true);
}

//------------------------------------------------------
LogViewer::~LogViewer()
{
    if (mLog != NULL) delete mLog;
    delete ui;
}

//------------------------------------------------------
void LogViewer::UpdateGraph(void)
{
    ui->customPlot->clearGraphs();
    double minValue =  10000000.0;
    double maxValue = -10000000.0;
    double startTime =  10000000000.0;
    double endTime   =  0.0;

    unsigned a;
    for (a = 0; a < mLog->NumItems(); a++)
    {
        if (!mLog->ItemDisplay(a)) continue;

        QVector<double> time;
        QVector<double> data;
        time.reserve(mLog->ItemSamples(a));
        data.reserve(mLog->ItemSamples(a));
        unsigned s;
        for (s = 0; s < mLog->ItemSamples(a); s++)
        {
            RacedashLog::Sample samp = mLog->ItemSample(a, s);
            time.append(samp.time);
            data.append(samp.value);
            if      (samp.value < minValue) minValue = samp.value;
            else if (samp.value > maxValue) maxValue = samp.value;
        }
        QCPGraph *g = ui->customPlot->addGraph();
        g->addData(time, data, true);
        //g->setBrush(sBrushes[a % NUM_BRUSHES]);

        if (!time.empty())
        {
            if (time[0] < startTime) startTime = time[0];
            if (time[time.size() - 1] > endTime) endTime = time[time.size() - 1];
        }
    }

    ui->customPlot->xAxis->setRange(startTime, endTime);
    ui->customPlot->yAxis->setRange(minValue, maxValue);
    ui->customPlot->replot();
}

//------------------------------------------------------
void LogViewer::on_actionOpen_triggered()
{
    QString filename = QFileDialog::getOpenFileName(this,
        "Open Racedash Log", "",
        "RaceDash Log Files (*.rdl);;ToothLog Files (*.csv);;All Files (*)");

    if (filename.contains(".rdl", Qt::CaseInsensitive))
    {
        mLog = new RacedashLog(filename.toLatin1());
    }

    if (filename.contains(".csv", Qt::CaseInsensitive))
    {
        mLog = new ToothLog(filename.toLatin1());
    }

    if ((mLog == NULL) || (!mLog->IsValid()))
    {
        if (mLog != NULL)
        {
            delete mLog;
            mLog = NULL;
        }
        statusBar()->showMessage("Log file load failed.");
        return;
    }

    QString m = QString("Log loaded %ld samples.").arg(mLog->TotalSamples());
    statusBar()->showMessage(m);

    // Add loaded channels

    ui->itemList->clear();
    ui->itemList->setUniformItemSizes(false);
    ui->itemList->setSizeAdjustPolicy(QListWidget::AdjustToContents);

    unsigned a;
    for (a = 0; a < mLog->NumItems(); a++)
    {
        QListWidgetItem *lwi = new QListWidgetItem(mLog->ItemName(a));
        lwi->setData(Qt::UserRole, a);
        lwi->setFlags(lwi->flags() | Qt::ItemIsUserCheckable); // set checkable flag

        if (mLog->ItemDisplay(a) && (mLog->ItemSamples(a) != 0)) lwi->setCheckState(Qt::Checked);
        else                                                     lwi->setCheckState(Qt::Unchecked);

        ui->itemList->addItem(lwi);
    }
    ui->itemList->sortItems();
    ui->itemList->adjustSize();

    UpdateGraph();
}

//-----------------------------------------------------------
void LogViewer::on_itemList_itemChanged(QListWidgetItem *item)
{
    unsigned itemIndex = item->data(Qt::UserRole).toInt();
    if (itemIndex < mLog->NumItems())
    {
        mLog->SetItemDisplay(itemIndex, item->checkState() == Qt::Checked);
    }
    UpdateGraph();
}
