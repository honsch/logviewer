#include "log.h"

#include <stdlib.h>
#include <string.h>

const char *Log::LogTypeNames[8] = { "Double", "Float", "U32", "S32", "U16", "S16", "U8", "S8" };

//----------------------------------------------
Log::Log(void) :
    mTotalSamples(0),
    mTotalTimeIndices(0)
{
}

//----------------------------------------------
Log::~Log()
{
    mTotalSamples = 0;
    while (!mLogItems.empty())
    {
        LogItem &i = mLogItems.back();
        free(i.name);
        free(i.units);
        mLogItems.pop_back();
    }
}

//----------------------------------------------------
int Log::FindLogItemByID(DataLoggerID id)
{
    unsigned a;
    for (a = 0; a < mLogItems.size(); a++)
    {
        if (mLogItems[a].id == id) return (int) a;
    }
    return -1;
}
