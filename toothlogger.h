#ifndef TOOTHLOGGER_H
#define TOOTHLOGGER_H
#include "log.h"

//--------------------------------------------
class ToothLog : public Log
{
  public:
    ToothLog(const char *fname);
   ~ToothLog();

  private:
    unsigned ParseLine(char *line, char *items[]);


};

#endif // TOOTHLOGGER_H
