#ifndef LOG_H
#define LOG_H

#include <stdint.h>
#include <stdio.h>
#include <vector>

//--------------------------------------------
class Log
{
  public:
    Log(void);
    virtual ~Log();

    struct Sample
    {
        double time;
        double value;
    };

    bool          IsValid(void) const { return mTotalSamples > 0; }

    unsigned      NumItems(void)     const { return mLogItems.size(); }
    unsigned      TotalSamples(void) const { return mTotalSamples; }
    unsigned      TotalTimes(void)   const { return mTotalTimeIndices; }

    const char   *ItemName(unsigned itemIndex)  const { return mLogItems[itemIndex].name; }
    const char   *ItemUnits(unsigned itemIndex) const { return mLogItems[itemIndex].units; }
    const char   *ItemType(unsigned itemIndex)  const { return LogTypeNames[mLogItems[itemIndex].type]; }
    unsigned      ItemSamples(unsigned itemIndex) const { return mLogItems[itemIndex].samples.size(); }
    const Sample &ItemSample(unsigned itemIndex, unsigned sampleIndex) const { return mLogItems[itemIndex].samples[sampleIndex]; }

    bool          ItemDisplay(unsigned itemIndex) const { return mLogItems[itemIndex].display; }
    void          SetItemDisplay(unsigned itemIndex, bool en) { mLogItems[itemIndex].display = en; }

  protected:

    typedef uint32_t DataLoggerID;
    enum LogVariableType { Double, Float, U32, S32, U16, S16, U8, S8 };
    static const char *LogTypeNames[8];

    // Internal use
    struct LogItem
    {
        char        *name;
        char        *units;
        bool         display;
        double       scale;
        uint32_t     type;
        DataLoggerID id;
        std::vector<Sample> samples;
    };

    int FindLogItemByID(DataLoggerID id);

    std::vector<LogItem> mLogItems;
    unsigned mTotalSamples;
    unsigned mTotalTimeIndices;
};

#endif // LOG_H
